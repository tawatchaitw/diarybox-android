package com.diaryportal.diarybox;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {


    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        RecyclerView rv = view.findViewById(R.id.recycler_view_main);
        rv.setHasFixedSize(true);

        List<Content> contents = new ArrayList<>();
        contents.add(new Content("aaa", "description", ""));
        contents.add(new Content("bbb", "description", ""));
        contents.add(new Content("ccc", "description", ""));
        contents.add(new Content("ddd", "description", ""));
        contents.add(new Content("eee", "description", ""));
        contents.add(new Content("fff", "description", ""));
        contents.add(new Content("ggg", "description", ""));
        contents.add(new Content("hhh", "description", "add"));

        MainAdapter adapter = new MainAdapter(contents);
        rv.setAdapter(adapter);

        rv.setLayoutManager(new GridLayoutManager(getContext(),3));

        return view;
    }

}
