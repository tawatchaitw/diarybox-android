package com.diaryportal.diarybox;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private Context mContext;
    private List<Content> mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView mCardView;
        public TextView mTextView;
        public ImageView mImageView;
        public ViewHolder(View v) {
            super (v);

            mCardView = v.findViewById(R.id.card_view_content);
            mTextView = v.findViewById(R.id.textView);
            mImageView = v.findViewById(R.id.imageView);
        }
    }

    public MainAdapter(List<Content> dataset) {
        mDataset = dataset;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_content, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        mContext = parent.getContext();

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(mDataset.get(position).getIcon() == "add"){
            holder.mImageView.setImageResource(R.drawable.ic_outline_add_circle_outline_24px);
            holder.mTextView.setTextColor(ContextCompat.getColor(mContext, R.color.colorLightGray));
        }
        else {
            holder.mImageView.setImageResource(R.drawable.ic_outline_folder_24px);
            holder.mTextView.setTextColor(ContextCompat.getColor(mContext, R.color.colorLightGray));
        }

        holder.mCardView.setOnClickListener((View view) -> {

        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


}
