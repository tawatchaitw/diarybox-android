package com.diaryportal.diarybox;

public class Content {
    private String Name;
    private String Description;
    private String Icon;

    public Content(String name, String description, String icon) {
        Name = name;
        Description = description;
        Icon = icon;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }
}
